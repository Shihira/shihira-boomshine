
/********************************************\
 * The file is created by Binary to Header. *
 * Binary_to_header's author is Shihira.    *
 * I Have opened it's source code.          *
 * E-mail : fengzhiping@hotmail.com         *
\********************************************/

#include "points.h"
#include "boomshine_ui.h"
#include "cake.png.h"
#include "icon.png.h"

game_ui::game_ui(QWidget *parent)
        : QWidget(parent)
{
        setFixedSize(400, 600);

        m_score = 0;
        m_game_index = 0;

        show_state = new QLabel(this);
        show_state->setStyleSheet(
                "font-family: Arial;"
                "font-size: 15px;"
                "padding-left: 50px;"
                "padding-right: 50px;"
                "padding-top: 10px;"
                "padding-bottom: 10px;"
                "background-color: rgba(0, 0, 0, 127);"
                "color: rgb(255, 255, 255);"
        );
        show_state->show();

        player = 0;
        change_to_display();

        setStyleSheet("background-color: rgb(220, 238, 238);");
        setWindowTitle("Shihira Boomshine");

        QPixmap icon;
        icon.loadFromData(::icon_png, ::icon_png_size);
        setWindowIcon(icon);
}

void game_ui::paintEvent(QPaintEvent *e)
{
        QPainter painter(this);
        if(m_game_index == sizeof(::total) / sizeof(int) && mode == display) {
                QPixmap picture;
                picture.loadFromData(::cake_png, ::cake_png_size);
                painter.drawPixmap(0, 0, picture);
        }
}


#ifndef  bomb_INC
#define  bomb_INC

#include <vector>

#include <stdlib.h>
#include <time.h>

#define rand_bet(a, b) (rand() % (b - a + 1) + a)
#define frand_bet(a, b) (rand() / (float)RAND_MAX + rand() % (b - a) + a)

class bomb {
        friend class boomshine_ui;
        friend class game_ui;

        typedef std::vector<const bomb*> blacklist_t;
        blacklist_t blacklist;

        float m_vx;
        float m_vy;
        float m_vr;

        int m_left;
        int m_top;
        int m_right; // the edge of parent
        int m_bottom;
public:
        enum {
                normal = 0,
                expanding = 1,
                standstill = 2,
                shrinking = 3,
                disappeared = 4
        } state;

        float r;
        float x;
        float y;
        unsigned long c/*olor*/;

        int standstill_timeout;
        int shrinking_timeout;
        int expanding_timeout;

        bomb(int width, int height) {
                state = normal;

                m_top = 0;
                m_left = 0;
                m_right = width;
                m_bottom = height;

                x = rand_bet(0, width);
                y = rand_bet(0, height);
                c = rand_bet(0x55, 0xAA) << 24 | rand_bet(0, 0xff) << 16 | rand_bet(0, 0xffff);
                r = 5;
                m_vx = frand_bet(1, 5) * (rand_bet(0, 1) > 0 ? 1.0 : -1.0);
                m_vy = frand_bet(1, 5) * (rand_bet(0, 1) > 0 ? 1.0 : -1.0);
                expanding_timeout = 30;
                standstill_timeout = rand_bet(50, 80);
                m_vr = frand_bet(2, 4);
        }

        void add_blacklist(const bomb* exploded) {
                blacklist.push_back(exploded);
        }

        bool check_blacklist() {
                for(blacklist_t::iterator i = blacklist.begin();
                        i != blacklist.end(); i++) {
                        if(! *i) continue;
                        const bomb& eb = **i;

                        if(eb.state == disappeared)
                                *i = 0;

                        int a = eb.x - x,
                            b = eb.y - y,
                            c = eb.r + r; 
                        if(a * a + b * b < c * c) {
                                state = expanding;
                                blacklist.clear();
                                return true;
                        }
                }
                return false;
        }

        bool tick() { // if state changed
                // printf("state[%d]\n", state);
                if(state == normal) {
                        if(x >= m_right || x <= m_left) m_vx = -m_vx;
                        if(y >= m_bottom || y <= m_top) m_vy = -m_vy;

                        if(x > m_right) x = m_right;
                        if(x < m_left) x = m_left;
                        if(y > m_bottom) y = m_bottom;
                        if(y < m_top) y = m_top;

                        x += m_vx;
                        y += m_vy;

                        return check_blacklist();
                }

                if(state == expanding) {
                        if(expanding_timeout <= 0) {
                                state = standstill;
                                return true;
                        }

                        expanding_timeout --;
                        if(m_vr > 0) m_vr -= 0.05;
                        else m_vr = 0;
                        r += m_vr;
                }

                if(state == standstill) {
                        if(standstill_timeout <= 0) {
                                state = shrinking;
                                return true;
                        }
                        standstill_timeout--;
                }

                if(state == shrinking) {
                        if(r <= 0) {
                                state = disappeared;
                                return true;
                        }

                        shrinking_timeout --;
                        m_vr += 0.05;
                        if(m_vr > r) m_vr = r;
                        r -= m_vr;
                }

                if(state == disappeared) {
                        r = 0;
                }

                return false;
        }
};

#endif   /* ----- #ifndef bomb_INC  ----- */


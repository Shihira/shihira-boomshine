#ifndef  boomshine_ui_INC
#define  boomshine_ui_INC

#include <QtGui>
#include <vector>

#include "bomb.h"

class boomshine_ui : public QWidget {
        Q_OBJECT

        friend class game_ui;

        typedef std::vector<bomb> bomb_list_t;
        bomb_list_t bomb_list;
        void hurry_notification(bomb* eb);

protected:
        void timerEvent(QTimerEvent* e);
        void paintEvent(QPaintEvent* e);
        void mousePressEvent(QMouseEvent* e);

        int m_score;

public:
        boomshine_ui(int bomb_num, QWidget* parent);
        ~boomshine_ui();

signals:
        void all_clear();
        void score_change(int);
};

///////////////////////////////////////////////////////////
// game_ui

class game_ui : public QWidget {
        Q_OBJECT

        int m_score;
        int m_game_index;

        boomshine_ui *player;
        QLabel* show_state;

        enum { display = 0, play = 1 } mode;
public:
        game_ui(QWidget *parent = 0);

protected:
        void paintEvent(QPaintEvent* e);

private slots:
        void change_to_play();
        void change_to_display();

        void score_change(int);
};

#endif   /* ----- #ifndef boomshine_ui_INC  ----- */

#include "boomshine_ui.h"
#include "points.h"

void game_ui::change_to_play()
{
        if(player) {
                disconnect(player, SIGNAL(all_clear()), this, SLOT(change_to_play()));
                disconnect(player, SIGNAL(score_change(int)), this, SLOT(score_change(int)));
                delete player;
        }

        player = new boomshine_ui(::total[m_game_index], this);
        if(m_game_index == sizeof(::total) / sizeof(int) - 1)
                for(int i = 0; i < player->bomb_list.size(); i++) {
                        player->bomb_list[i].x = ::points[i * 2];
                        player->bomb_list[i].y = ::points[i * 2 + 1];
                        player->bomb_list[i].m_vx /= 4;
                        player->bomb_list[i].m_vy /= 4;
                        player->bomb_list[i].m_left = ::points[i * 2] - 2;
                        player->bomb_list[i].m_right = ::points[i * 2] + 2;
                        player->bomb_list[i].m_top = ::points[i * 2 + 1] - 2;
                        player->bomb_list[i].m_bottom = ::points[i * 2 + 1] + 2;
                }

        player->show();
        show_state->raise();

        connect(player, SIGNAL(all_clear()), this, SLOT(change_to_display()));
        connect(player, SIGNAL(score_change(int)), this, SLOT(score_change(int)));
        mode = play;
}

void game_ui::score_change(int score)
{
        static int last_score = 0;

        if(mode == play) {
                show_state->setText(QString::number(score)
                        + "/" + QString::number(::require[m_game_index]));
                show_state->adjustSize();
                show_state->move((width() - show_state->width()) / 2, 20);
                last_score = score;
        }

        if(mode == display) {

                QString str;

                if(m_game_index == sizeof(::total) / sizeof(int) - 1) {
                        str += "Happy Birthday to Siwan Lei!";
                        m_game_index++;
                } else if(last_score < ::require[m_game_index]) {
                        m_score -= last_score;
                        str = "YOU LOST.\n\nScore: %1/%2\nTotal: %3";
                        str = str.arg(QString::number(last_score),
                                QString::number(::require[m_game_index]),
                                QString::number(m_score));
                } else {
                        if(m_game_index != 0) str = "GOOD JOB!\n\n";
                        else str = "Click the screen to start a chain reaction\n"
                                "and try to burst as many dots as needed\n"
                                "to pass a level.\n\n";
                        str += "Score: %1/%2\nTotal: %3\n\nNext level: %4/%5";
                        str = str.arg(QString::number(last_score),
                                QString::number(::require[m_game_index]),
                                QString::number(m_score),
                                QString::number(::require[m_game_index + 1]),
                                QString::number(::total[m_game_index + 1]));
                        /*if(m_game_index == 0)
                                m_game_index = sizeof(::total) / sizeof(int) - 1;
                        else*/ m_game_index++;
                }

                show_state->setText(str);
                show_state->adjustSize();
                show_state->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
                show_state->move((width()- show_state->width()) / 2, 80);
        }
}

void game_ui::change_to_display()
{
        if(player) {
                m_score += player->m_score;
                disconnect(player, SIGNAL(all_clear()), this, SLOT(change_to_display()));
                disconnect(player, SIGNAL(score_change(int)), this, SLOT(score_change(int)));
                delete player;
        }

        player = new boomshine_ui(30, this);
        player->show();
        show_state->raise();

        for(int i = 0; i < player->bomb_list.size(); i++) {
                player->bomb_list[i].state = bomb::expanding;
                player->bomb_list[i].expanding_timeout = 80;
                player->bomb_list[i].standstill_timeout = 0;
        }

        if(m_game_index != sizeof(::total) / sizeof(int) - 1) {
                connect(player, SIGNAL(all_clear()), this, SLOT(change_to_play()));
        }

        connect(player, SIGNAL(score_change(int)), this, SLOT(score_change(int)));
        mode = display;
}

int main(int argc, char* argv[])
{
        srand((int)time(0));

        QApplication app(argc, argv);
        game_ui window;
        window.show();
        app.exec();
}

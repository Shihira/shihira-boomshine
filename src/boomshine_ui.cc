#include <QtGui>
#include "boomshine_ui.h"
#include "boomshine_ui.moc"

#include "points.h"

boomshine_ui::boomshine_ui(int bomb_num, QWidget* parent)
        : QWidget(parent)
{
        m_score = 0;

        setFixedSize(parent->size());

        for(int i = 0; i < bomb_num; i++)
                bomb_list.push_back(bomb(width(), height()));
        startTimer(30);
}

boomshine_ui::~boomshine_ui() { }

void boomshine_ui::hurry_notification(bomb *eb)
{
        for(bomb_list_t::iterator i = bomb_list.begin();
                i != bomb_list.end(); i++) {
                if(i->state != bomb::normal) continue;
                i->add_blacklist(eb);
        }
}

void boomshine_ui::timerEvent(QTimerEvent *e)
{
        int num;
        int nor_num = 0;
        int exp_num = 0;
        int dis_num = 0;

        for(bomb_list_t::iterator i = bomb_list.begin();
                i != bomb_list.end(); i++) {
                bool sc = i->tick(); // state changed?

                if(i->state == bomb::expanding && sc)
                        hurry_notification(&(*i));

                if(i->state == bomb::normal)
                        nor_num ++;
                else if(i->state == bomb::disappeared)
                        dis_num ++;
                else    exp_num++;
        }

        if((num = dis_num + exp_num) != m_score + 1) {
                if(num <= 0) num = 1;
                emit score_change(num - 1);
        }

        m_score = dis_num + exp_num - 1;

        if(nor_num + dis_num == bomb_list.size() && dis_num)
                emit all_clear();

        update();
}

void boomshine_ui::mousePressEvent(QMouseEvent* e)
{
        if(e->button() == Qt::LeftButton && m_score == -1) {
                bomb_list.push_back(bomb(width(), height()));
                bomb_list_t::iterator i = bomb_list.end() - 1;

                i->x = e->x();
                i->y = e->y();
                i->state = bomb::expanding;

                hurry_notification(&(*i));
        }/* else if(e->button() == Qt::RightButton) {
                bomb_list.push_back(bomb(width(), height()));
                bomb_list_t::iterator i = bomb_list.end() - 1;

                i->x = e->x();
                i->y = e->y();
                i->m_vx /= 4;
                i->m_vy /= 4;
                i->m_left = e->x() - 2;
                i->m_right = e->x() + 2;
                i->m_top = e->y() - 2;
                i->m_bottom = e->y() + 2;

                printf("        %d, %d,\n", e->x(), e->y());
        }*/
}

void boomshine_ui::paintEvent(QPaintEvent *e)
{
        QPainter painter(this);
        painter.setRenderHint(QPainter::Antialiasing, true);
        //painter.fillRect(QRect(QPoint(0, 0), size()), QBrush(0xffcccccc));
        painter.setPen(0xffffffff);

        for(bomb_list_t::iterator i = bomb_list.begin();
                i != bomb_list.end(); i++) {
                int red = i->c << 8 >> 24,
                    green = i->c << 16 >> 24,
                    blue = i->c << 24 >> 24,
                    alpha = i->c << 0 >> 24;
                painter.setBrush(QBrush(QColor(red, green, blue, alpha)));

                int x = i->x;
                int y = i->y;
                int r = i->r;
                painter.drawEllipse(x - r, y - r, 2 * r, 2 * r);
        }
}
